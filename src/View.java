import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class View extends Application{

	Model m = new Model();
	boolean click = false;
	double x;
	double y;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Group root = new Group();
		Scene scene = new Scene(root);
		primaryStage.show();
		primaryStage.setScene(scene);
		
		
		scene.setOnMousePressed(new EventHandler<MouseEvent>()
        {
            public void handle(MouseEvent e)
            {
                click = true;
                x = e.getX();
                y = e.getY();
            }
        });
		
		scene.setOnMouseReleased(new EventHandler<MouseEvent>()
        {
            public void handle(MouseEvent e)
            {
                click = false;
            }
        });
		
		new AnimationTimer() {
			@Override
			public void handle(long now) {
				root.getChildren().clear();
				if(click) {
					m.shot(x, y);
				}
				m.getList().forEach(e->e.update());
				m.getList().forEach(e->root.getChildren().add(new Rectangle(e.getX(), e.getY(), 10, 10)));
				
				
			}
		}.start();
	}
	
	public static void main(String[] args) {
        launch(args);
    }
}

