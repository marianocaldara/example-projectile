public class Proiettile {
	private double x;
	private double y;
	private double xMouse;
	private double yMouse;
	private double angle;
	private double speed = 4.0;
	
	
	public Proiettile(double xMouse, double yMouse) {
		this.x = 250;
		this.y = 500;
		this.xMouse=xMouse;
		this.yMouse=yMouse;
		this.angle = Math.toRadians(getAngle());
	}
	
	private double getAngle() {
		angle =  Math.toDegrees(Math.atan2(yMouse - y, xMouse - x));

	    if(angle < 0){
	        angle += 360;
	    }
	    
	    return angle;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public void update() {
		this.x += speed*Math.cos(angle);
		this.y += speed*Math.sin(angle);
	}
}
