import java.util.*;

public class Model {
	List<Proiettile> pro = new LinkedList<>();

	public List<Proiettile> getList(){
		return Collections.unmodifiableList(pro);
	}

	public void shot(double xMouse, double yMouse) {
		
		pro.add(new Proiettile(xMouse, yMouse));
		
	}
	
}
